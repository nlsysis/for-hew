#pragma once
#include "Window.h"
#include "GameTimer.h"
#include "ImguiManager.h"

class d3dApp
{
public:
	d3dApp();
	// master frame / message loop
	int Go();
	~d3dApp();
private:
	void DoFrame();
	void CalculateFrameStats(void) noexcept;
private:
	ImguiManager m_imgui;
	Window wnd;
	GameTimer m_Timer;
	bool      m_AppPaused;
	float    currentElapsed;
	float  speed_factor = 1.0f;
	std::vector<std::unique_ptr<class Box>> boxes;
	std::vector<std::unique_ptr<class Box2D>> box2D;
};