#include "WinMouse.h"


std::pair<int,int> WinMouse::GetPos() const noexcept
{
	return { x,y };
}

int WinMouse::GetPosX() const noexcept
{
	return x;
}

int WinMouse::GetPosY() const noexcept
{
	return y;
}

bool WinMouse::IsInWindow() const noexcept
{
	return isInWindow;
}

bool WinMouse::LeftIsPressed() const noexcept
{
	return leftIsPressed;
}

bool WinMouse::RightIsPressed() const noexcept
{
	return rightIsPressed;
}

WinMouse::Event WinMouse::Read() noexcept
{
	if( buffer.size() > 0u )
	{
		WinMouse::Event e = buffer.front();
		buffer.pop();
		return e;
	}
	else
	{
		return WinMouse::Event();
	}
}

void WinMouse::Flush() noexcept
{
	buffer = std::queue<Event>();
}

void WinMouse::OnMouseMove( int newx,int newy ) noexcept
{
	x = newx;
	y = newy;

	buffer.push( WinMouse::Event( WinMouse::Event::Type::Move,*this ) );
	TrimBuffer();
}

void WinMouse::OnMouseLeave() noexcept
{
	isInWindow = false;
	buffer.push(WinMouse::Event(WinMouse::Event::Type::Leave, *this));
	TrimBuffer();
}

void WinMouse::OnMouseEnter() noexcept
{
	isInWindow = true;
	buffer.push(WinMouse::Event(WinMouse::Event::Type::Enter, *this));
	TrimBuffer();
}

void WinMouse::OnLeftPressed( int x,int y ) noexcept
{
	leftIsPressed = true;

	buffer.push( WinMouse::Event( WinMouse::Event::Type::LPress,*this ) );
	TrimBuffer();
}

void WinMouse::OnLeftReleased( int x,int y ) noexcept
{
	leftIsPressed = false;

	buffer.push( WinMouse::Event( WinMouse::Event::Type::LRelease,*this ) );
	TrimBuffer();
}

void WinMouse::OnRightPressed( int x,int y ) noexcept
{
	rightIsPressed = true;

	buffer.push( WinMouse::Event( WinMouse::Event::Type::RPress,*this ) );
	TrimBuffer();
}

void WinMouse::OnRightReleased( int x,int y ) noexcept
{
	rightIsPressed = false;

	buffer.push( WinMouse::Event( WinMouse::Event::Type::RRelease,*this ) );
	TrimBuffer();
}

void WinMouse::OnWheelUp( int x,int y ) noexcept
{
	buffer.push( WinMouse::Event( WinMouse::Event::Type::WheelUp,*this ) );
	TrimBuffer();
}

void WinMouse::OnWheelDown( int x,int y ) noexcept
{
	buffer.push( WinMouse::Event( WinMouse::Event::Type::WheelDown,*this ) );
	TrimBuffer();
}

void WinMouse::TrimBuffer() noexcept
{
	while( buffer.size() > bufferSize )
	{
		buffer.pop();
	}
}