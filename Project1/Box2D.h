#pragma once
#include "DrawableBase.h"

class Box2D : public DrawableBase<Box2D>
{
public:
	Box2D(Graphics& gfx);
	void Update(float dt) noexcept override;
	DirectX::XMMATRIX GetTransformXM() const noexcept override;
private:
	float r;
	float roll = 0.0f;
	float pitch = 0.0f;
	float yaw = 0.0f;
	DirectX::XMFLOAT3X3 mt;
};