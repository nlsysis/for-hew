#pragma once
#include "IndexedTriangleList.h"
#include <DirectXMath.h>

class Polygon
{
public:
	template<class V>
	static IndexedTriangleList<V> Make()
	{
		std::vector<DirectX::XMFLOAT2> vertices;
		vertices.emplace_back(-0.5f, 0.5f);  //0
		vertices.emplace_back(0.5f, 0.5f);   //1
		vertices.emplace_back(-0.5f, -0.5f); //2
		vertices.emplace_back(0.5f, -0.5f);  //3

		std::vector<V> verts(vertices.size());
		for (size_t i = 0; i < vertices.size(); i++)
		{
			verts[i].pos = vertices[i];
		}
		return{
			std::move(verts),
			{0,1, 1,3,
			 3,2, 2,0}
		};
	}
};