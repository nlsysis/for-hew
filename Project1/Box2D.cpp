#include "Box2D.h"
#include "BindableBase.h"
#include "GraphicsThrowMacros.h"
#include "Polygon.h"

Box2D::Box2D(Graphics &gfx)
{
	namespace dx = DirectX;
	if ( !IsStaticInitialized() )
	{
		struct Vertex
		{
			dx::XMFLOAT2 pos;
		};

		const auto model = Polygon::Make<Vertex>();

		AddStaticBind(std::make_unique<VertexBuffer>(gfx, model.vertices));
		//put shader into pipeline
		auto pvs = std::make_unique<VertexShader>(gfx, L"VertexShader2D.cso");
		auto pvsbc = pvs->GetBytecode();
		AddStaticBind(std::move(pvs));

		AddStaticBind(std::make_unique<PixelShader>(gfx, L"PixelShader2D.cso"));


		AddStaticIndexBuffer(std::make_unique<IndexBuffer>(gfx, model.indices));

		const std::vector<D3D11_INPUT_ELEMENT_DESC> ied=
		{
			{"Position",0,DXGI_FORMAT_R32G32_FLOAT,0,0,D3D11_INPUT_PER_VERTEX_DATA,0},
		};
		AddStaticBind(std::make_unique<InputLayout>(gfx, ied, pvsbc));

		AddStaticBind(std::make_unique<Topology>(gfx, D3D_PRIMITIVE_TOPOLOGY_LINELIST));
	}
	else
	{
		SetIndexFromStatic();
	}
	
	AddBind(std::make_unique<TransformCbuf>(gfx, *this));
	DirectX::XMStoreFloat3x3(&mt, DirectX::XMMatrixScaling(1.0f, 1.0f, 0.8f));
}

void Box2D::Update(float dt) noexcept
{
	roll += dt;
	pitch += dt;
	yaw += dt;
}

DirectX::XMMATRIX Box2D::GetTransformXM() const noexcept
{

	return DirectX::XMLoadFloat3x3(&mt) *
		DirectX::XMMatrixRotationRollPitchYaw(pitch, yaw, roll);
}
