#include <sstream>
#include <memory>
#include "d3dApp.h"
#include "Box.h"
#include "Box2D.h"
#include "imgui/imgui.h"

d3dApp::d3dApp()
	:
	currentElapsed(0.0f),
	wnd(800, 600, "The Test for windows"),
	m_AppPaused(false)
{
	std::mt19937 rng(std::random_device{}());  //standard the random 
	std::uniform_real_distribution<float> adist(0.0f, 3.1415f * 2.0f);
	std::uniform_real_distribution<float> ddist(0.0f, 3.1415f * 2.0f);
	std::uniform_real_distribution<float> odist(0.0f, 3.1415f * 0.3f);
	std::uniform_real_distribution<float> rdist(6.0f, 20.0f);
	std::uniform_real_distribution<float> bdist{ 0.4f,3.0f };
	for (auto i = 0; i < 80; i++)
	{
		boxes.push_back(std::make_unique<Box>(
			wnd.Gfx(), rng, adist,
			ddist, odist, rdist,
			bdist
			));
	}
	wnd.Gfx().SetProjection(DirectX::XMMatrixPerspectiveLH(1.0f, 3.0f / 4.0f, 0.5f, 40.0f));

}

int d3dApp::Go()
{
	m_Timer.Reset();
	const float timePerFrame = 1.0f / 60.0f;

	while (true)
	{
		// process all messages pending, but to not block for new messages
		if (const auto ecode = Window::ProcessMessages())
		{
			// if return optional has value, means we're quitting so return exit code
			return *ecode;
		}

		m_Timer.Tick();
		if (!m_AppPaused)
		{
			//for steady the frame right can be m_Timer.DeltaTime() * 0.1f
			//but to setting FPS can use parament 'timePerFrame'.
			if (( m_Timer.TotalTime() - currentElapsed) > m_Timer.DeltaTime() * 0.1f)
			{
				CalculateFrameStats();
				DoFrame();
				currentElapsed = m_Timer.TotalTime();
			}
		}
		else
		{
			//Sleep(100);
			m_Timer.Stop();
		}
	}
}

d3dApp::~d3dApp()
{
}

void d3dApp::DoFrame()
{
	const auto dt = m_Timer.DeltaTime() * speed_factor;
	
	wnd.Gfx().BeginFrame(0.07f, 0.0f, 0.12f);
	for (auto& b : boxes)
	{
		b->Update(dt);
		b->Draw(wnd.Gfx());
	}

	static char buffer[1024];
	
	//imgui window to control simulation speed
	if (ImGui::Begin("Test speed"))
	{
		ImGui::SliderFloat("Speed Factor", &speed_factor, 0.0f, 4.0f);
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::InputText("Butts",buffer,sizeof(buffer));
	}
	ImGui::End();

	wnd.Gfx().EndFrame();
}

void d3dApp::CalculateFrameStats(void) noexcept
{
	static int frameCnt = 0;
	static float timeElapsed = 0.0f;
	frameCnt++;

	if ((m_Timer.TotalTime() - timeElapsed) >= 1.0f)
	{
		float fps = (float)frameCnt; // fps = frameCnt / 1
		float mspf = 1000.0f / fps;
		std::ostringstream outs;
		outs.precision(5);
		outs <<  "    "
			<< "FPS: " << fps << "    "
			<< "Frame Time: " << mspf << " (ms)";

		wnd.SetTitle( outs.str());

		/// Reset for next average.
		frameCnt = 0;
		//timeElapsed += 1.0f;
		timeElapsed = m_Timer.TotalTime();
		
	}
}