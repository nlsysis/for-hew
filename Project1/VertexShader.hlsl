
cbuffer cBuf
{
	//row_major matrix transform;   //row_major----one way to translate the directx matrix to HLSL form
	matrix transform;
};

float4 main(float3 pos : Position1) : SV_Position
{
	return mul( float4(pos,1.0f),transform);
}